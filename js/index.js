	$(function(){
		$("[data-toggle='tooltip']").tooltip();
		$("[data-toggle='popover']").popover();
		$('.carousel').carousel({
			interval:2000
		});

	//eventos en consola BTN Más
	$('#detalles').on('show.bs.modal', function(e){
		console.log('El Modal Más se está mostrando');
		$('#btnMas').removeClass('btn-outline-success');
		$('#btnMas').addClass('btn-primary');
		$('#btnMas').prop('disabled', true);

	});
	$('#detalles').on('shown.bs.modal', function(e){
		console.log('El Modal Más se mostró');
		$('#btnMas').prop('disabled', true);
	});
	$('#detalles').on('hide.bs.modal', function(e){
		console.log('El Modal Más se oculta');
	});
	$('#detalles').on('hidden.bs.modal', function(e){
		console.log('El Modal Más se ocultó');
		$('#btnMas').prop('disabled', false);
		$('#btnMas').removeClass('btn-primary');
		$('#btnMas').addClass('btn-outline-success');
	});
	//Fin eventos en Consola

	//eventos en consola
	$('#contactoFast').on('show.bs.modal', function(e){
		console.log('El Modal contacto se está mostrando');
		$('#btnCont').removeClass('btn-outline-success');
		$('#btnCont').addClass('btn-primary');
		$('#btnCont').prop('disabled', true);

	});
	$('#contactoFast').on('shown.bs.modal', function(e){
		console.log('El Modal contacto se mostró');
	});
	$('#contactoFast').on('hide.bs.modal', function(e){
		console.log('El Modal contacto se oculta');
	});
	$('#contactoFast').on('hidden.bs.modal', function(e){
		console.log('El Modal contacto se ocultó');
		$('#btnCont').prop('disabled', false);
		$('#btnCont').removeClass('btn-primary');
		$('#btnCont').addClass('btn-outline-success');
	});

	//Fin eventos en Consola


});
